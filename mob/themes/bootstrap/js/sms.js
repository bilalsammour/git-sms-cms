function sendSms(to, body) {
	if(/Android/i.test(navigator.userAgent)) {
		window.location.href = "sms:" + to + "?body=" + body;

	} else if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {
		window.location.href = "sms:" + to + "&body=" + body;

	} else {
		window.location.href = "sms:" + to + "?body=" + body;
	}
}