<script type='text/javascript'>
	$(document).ready(function() {
		$("#Service_body,#Service_to").on("keyUp change",function() {
			var body = $("#Service_body").val();
			var to = $("#Service_to").val();
			if(body.length > 0  &&  to.length > 0) {
				console.log("inside");
				CKEDITOR.instances.Service_output_text.setData('<h2 style="text-align: center;">\
	ان لم يفتح تطبيق الرسائل تلقائيا، اضغط على الرابط ادناه</h2>\
<h2 style="text-align: center;">\
	<a href="sms:' + to + '?body=' + body + '">أرسل</a></h2>\
<h2 style="text-align: center;">\
	او ارسل ' + body + ' للرقم ' + to + '</h2>\
');
			}
		})
	})
</script>
<div class="form">

<?php $form=$this->beginWidget('BActiveForm', array(
	'id'=>'service-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php $this->widget('BAlert',array(

		'content'=>'<p>Fields with <span class="required">*</span> are required.</p>'
	)); ?>

	<?php echo $form->errorSummary($model); ?>

	<div class="<?php echo $form->fieldClass($model, 'name'); ?>">
		<?php echo $form->labelEx($model,'name'); ?>
		<div class="input">
			<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
			<?php echo $form->error($model,'name'); ?>
		</div>
	</div>

	<div class="<?php echo $form->fieldClass($model, 'to'); ?>">
		<?php echo $form->labelEx($model,'to'); ?>
		<div class="input">
			<?php echo $form->textField($model,'to',array('size'=>45,'maxlength'=>45)); ?>
			<?php echo $form->error($model,'to'); ?>
		</div>
	</div>

	<div class="<?php echo $form->fieldClass($model, 'body'); ?>">
		<?php echo $form->labelEx($model,'body'); ?>
		<div class="input">
			<?php echo $form->textArea($model,'body',array('rows'=>6, 'cols'=>50)); ?>
			<?php echo $form->error($model,'body'); ?>
		</div>
	</div>

	<div class="<?php echo $form->fieldClass($model, 'output_text'); ?>">
		<?php echo $form->labelEx($model,'output_text'); ?>
		<div class="input">

				<?php $this->widget('application.extensions.eckeditor.ECKEditor', array(
	            'model'=>$model,
	            'attribute'=>'output_text',
	            'config' => array(
	                'toolbar'=>array(
	                    array( 'Bold', 'Italic', 'Underline', 'Strike' ),
	                    array( 'Source' ,'Link', 'Unlink', 'Anchor','CreatePlaceholder' ) ,
	                ),
	              ),
	            )); ?>
			<?php echo $form->error($model,'output_text'); ?>
		</div>
	</div>

	<div class="actions">
		<?php echo BHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->